import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    res: number;

    fibonacci(num: number): void {
        const worker = new Worker('./app.worker', { type: 'module' });
        worker.onmessage = ({ data }) => {
            console.log(data);
            this.res = data;
        };
        worker.postMessage(num);
    }
}
